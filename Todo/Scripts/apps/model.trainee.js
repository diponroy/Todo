﻿/*model*/
define('model.trainee',
    ['ko'],
    function (ko) {
        var trainee = function () {
            var self = this;
            self.id = ko.observable();
            self.name = ko.observable().extend({
                required: true,
                maxLength: 150,
            });
            self.contactNo = ko.observable().extend({
                maxLength: 20,
            });
            self.email = ko.observable().extend({
                maxLength: 100,
            });
            self.eventId = ko.observable().extend({
                required: true
            });
            self.status = ko.observable();

            self.toPostObj = function () {
                return {
                    id: self.id(),
                    name: self.name(),
                    contactNo: self.contactNo(),
                    email: self.email(),
                    eventId: self.eventId(),
                    status: self.status()
                };
            };

            self.load = function (obj) {
                self.id(obj.Id);
                self.name(obj.Name);
                self.contactNo(obj.ContactNo);
                self.email(obj.Email);
                self.eventId(obj.EventId);
                self.status(obj.Status);
            };

            self.reset = function () {
                self.id('');
                self.name('');
                self.contactNo('');
                self.email('');
                self.eventId('');
                self.status('');
            };
        };
        return trainee;
    });