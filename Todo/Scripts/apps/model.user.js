﻿/*model*/
define('model.user',
    ['jquery', 'ko'],
    function($, ko) {
        var user = function() {
            var self = this;
            self.id = ko.observable();
            self.name = ko.observable().extend({
                required: true,
                maxLength: 150,
            });
            self.contactNo = ko.observable().extend({
                maxLength: 20,
            });
            self.email = ko.observable().extend({
                required: true,
                email: true,
                maxLength: 100,
            });
            self.password = ko.observable().extend({
                required: true,
                maxLength: 50,
            });
            self.status = ko.observable().extend({
                required: true,
            });
            self.toPostObj = function() {
                return {
                    id: self.id(),
                    name: self.name(),
                    contactNo: self.contactNo(),
                    email: self.email(),
                    password: self.password(),
                    status: self.status()
                };
            };

            self.load = function(obj) {
                self.id(obj.Id);
                self.name(obj.Name);
                self.contactNo(obj.ContactNo);
                self.email(obj.Email);
                self.password(obj.Password);
                self.status(obj.Status);
            };

            self.reset = function () {
                self.id('');
                self.name('');
                self.contactNo('');
                self.email('');
                self.password('');
                self.status('');
            };
        };
        return user;
    });