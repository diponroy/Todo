﻿/*model*/
define('model.event',
    ['ko', 'model.device'],
    function(ko, deviceModel) {
        var event = function() {
            var self = this;
            self.id = ko.observable();
            self.name = ko.observable().extend({
                required: true,
                maxLength: 150,
            });
            self.deviceId = ko.observable().extend({
                required: true,
            });
            self.description = ko.observable().extend({
                maxLength: 150,
            });
            self.status = ko.observable().extend({
                required: true,
            });
            self.device = new deviceModel();

            self.toPostObj = function() {
                return {
                    id: self.id(),
                    name: self.name(),
                    deviceId: self.deviceId(),
                    description: self.description(),
                    status: self.status()
                };
            };

            self.load = function(obj) {
                self.id(obj.Id);
                self.name(obj.Name);
                self.deviceId(obj.DeviceId);
                self.description(obj.Description);
                self.status(obj.Status);
            };

            self.reset = function() {
                self.id('');
                self.name('');
                self.deviceId('');
                self.description('');
                self.status('');
            };
        };
        return event;
    });