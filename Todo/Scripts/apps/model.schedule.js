﻿/*model*/
define('model.schedule',
    ['ko', 'moment'],
    function (ko, moment) {
        var device = function () {
            var self = this;
            self.id = ko.observable();
            self.eventId = ko.observable();
            self.startingDateTime = ko.observable(new Date());
            self.endingDateTime = ko.observable(new Date());
            self.remarks = ko.observable();
            self.status = ko.observable();

            self.toPostObj = function () {
                return {
                    id: self.id(),
                    eventId: self.eventId(),
                    startingDateTime: moment(self.startingDateTime()).format('YYYY/MM/DD HH:mm'),
                    endingDateTime: moment(self.endingDateTime()).format('YYYY/MM/DD HH:mm'),
                    remarks: self.remarks(),
                    status: self.status(),
                };
            };

            self.load = function (obj) {
                self.id(obj.Id);
                self.eventId(obj.EventId);
                self.startingDateTime(moment(obj.StartingDateTime).toDate());
                self.endingDateTime(moment(obj.EndingDateTime).toDate());
                self.remarks(obj.Remarks);
                self.status(obj.Status);
            };

            self.reset = function () {
                self.id('');
                self.eventId('');
                self.startingDateTime(moment());
                self.endingDateTime(moment());
                self.remarks('');
                self.status('');
            };
        };
        return device;
    });