﻿define('vm.login',
    [
        'config',
        'notify',
        'ko',
        'model.user',
        'dataservice.login'
    ],
    function(config, notify, ko, model, service) {

        var self = this;
        self.model = new model();

        self.errors = ko.validation.group([self.model.email, self.model.password]);
        self.isBusy = ko.observable(false);

        self.signIn = function() {
            if (self.errors().length > 0) {
                self.errors.showAllMessages();
                return;
            }

            var obj = {
                entity: self.model.toPostObj()
            };
            self.isBusy(true);
            service.login({
                success: function(json) {
                    self.isBusy(false);
                    if (json) {
                        self.reset();
                        notify.success('login done.');
                        window.location = '/schedule/index';
                    }
                },
                error: function() {
                    self.isBusy(false);
                    notify.error('error to login user.');
                }
            }, obj);
        };

        self.reset = function() {
            self.model.email('');
            self.model.password('');
            self.errors.showAllMessages(false);
        };

        self.init = function() {
            self.model.email('Admin@gmail.com');
            self.model.password('Admin');
        };

        self.init();

        return self;
    });



