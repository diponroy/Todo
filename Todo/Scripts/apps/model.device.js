﻿/*model*/
define('model.device',
    ['ko'],
    function (ko) {
        var device = function () {
            var self = this;
            self.id = ko.observable();
            self.name = ko.observable().extend({
                required: true,
                maxLength: 150,
            });
            self.description = ko.observable().extend({
                maxLength: 300,
            });
            self.status = ko.observable().extend({
                required: true,
            });

            self.toPostObj = function () {
                return {
                    id: self.id(),
                    name: self.name(),
                    description: self.description(),
                    status: self.status()
                };
            };

            self.load = function (obj) {
                self.id(obj.Id);
                self.name(obj.Name);
                self.description(obj.Description);
                self.status(obj.Status);
            };

            self.reset = function () {
                self.name('');
                self.description('');
                self.status('');
            };
        };
        return device;
    });