﻿define('vm.schedule',
    [
        'jquery',
        'moment',
        'config',
        'notify',
        'model.schedule',
        'dataservice.event',
        'dataservice.schedule'
    ],
    function($, moment, config, notify, scheduleModel, eventService, scheduleService) {

        var self = this;
        self.events = ko.observableArray([]);
        self.schedules = ko.observableArray([]);
        self.loadingEvents = ko.observable(false);
        self.loadingSchedules = ko.observable(false);

        self.showModal = ko.observable(false);
        self.scheduleModalBusy = ko.observable(false);
        self.scheduleModel = new scheduleModel();
        self.errors = ko.validation.group(self.scheduleModel);
        self.allStatus = ko.observableArray([
            { text: 'Active', value: 0 },
            { text: 'Inactive', value: 1 }
        ]);
        
        self.loadEvents = function () {
            self.loadingEvents(true);
            self.events([]);

            return eventService.all({
                success: function (data) {
                    self.loadingEvents(false);
                    var arr = [];
                    $.each(data, function (index, item) {
                        arr.push({
                            name: item.Name,
                            dataEvent: JSON.stringify({
                                id: item.Id,
                                title: item.Name,
                                stick: true,
                            })
                        });
                    });
                    self.events(arr);


                    $('#external-events .fc-event').each(function () {
                        $(this).draggable({
                            zIndex: 999,
                            revert: true,      // will cause the event to go back to its
                            revertDuration: 0  //  original position after the drag
                        });
                    });

                },
                error: function () {
                    self.loadingEvents(false);
                    notify.error('error to load events.');
                }
            });
        };
        
        self.loadSchedules = function () {
            self.loadingSchedules(true);
            self.schedules([]);

            return scheduleService.all({
                success: function (data) {
                    self.loadingSchedules(false);
                    var arr = [];
                    $.each(data, function (index, item) {
                        var aSchedule = {
                            id: item.Id,
                            eventId: item.EventId,
                            startingDateTime: item.StartingDateTime,
                            endingDateTime: item.EndingDateTime,
                            status: item.status,
                            remarks: item.remarks,
                            
                            /*for fullcallender*/
                            title: item.Event.Name,
                            start: moment(item.StartingDateTime),
                            end: moment(item.EndingDateTime),
                        };
                        arr.push(aSchedule);
                    });
                    self.schedules(arr);
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar').fullCalendar('addEventSource', self.schedules());
                },
                error: function () {
                    self.loadingSchedules(false);
                    notify.error('error to load schedules.');
                }
            });
        };
        
        self.loadSchedule = function (id) {
            self.scheduleModalBusy(true);
            scheduleService.single({
                success: function (data) {
                    self.scheduleModel.load(data);
                    self.errors.showAllMessages(false);
                    self.scheduleModalBusy(false);
                },
                error: function () {
                    self.scheduleModalBusy(false);
                    self.showModal(false);
                    notify.error('error to load device.');
                }
            }, id);
        };
        
        self.showSchedules = function () {
            /* initialize the calendar*/
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                droppable: true, // this allows things to be dropped onto the calendar

                /*event list to schedule calender*/
                drop: function (date, jsEvent, ui) {
                    var aSchedule = new scheduleModel();
                    aSchedule.startingDateTime(moment(date).toDate());
                    aSchedule.endingDateTime(moment(date).toDate());
                    aSchedule.eventId($(this).data('event').id);
                    aSchedule.status(0);

                    scheduleService.create({
                        success: function (data) {
                            notify.success('event added to the date.');
                            self.loadSchedules();
                        },
                        error: function () {
                            notify.error('error to add event.');
                        }
                    }, aSchedule.toPostObj());
                },

                /*change dates of a scheduled event*/
                eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                    /*prevent to add event*/
                    revertFunc();
                    
                    self.scheduleModel.id(event.id);
                    self.scheduleModel.eventId(event.eventId);
                    self.scheduleModel.remarks(event.remarks);
                    self.scheduleModel.status(event.status);

                    var startingDate, endingDate = null;
                    startingDate = moment(event.startingDateTime).add(delta.asDays(), 'days').toDate();
                    endingDate = moment(event.endingDateTime).add(delta.asDays(), 'days').toDate();
                    self.scheduleModel.startingDateTime(startingDate);
                    self.scheduleModel.endingDateTime(endingDate);
                    
                    notify.confirm('Do you want to change the schedule?', function (confirmed) {
                        if (confirmed) {
                            self.update();
                        }
                    });
                },

                /*clicked on scheduled event*/
                eventClick: function (calEvent, jsEvent, view) {
                    self.showModal(true);
                    self.loadSchedule(calEvent.id);
                }
            });
        };


        self.remove = function (id) {
            self.scheduleModalBusy(true);
            scheduleService.remove({
                success: function (data) {
                    self.scheduleModalBusy(false);
                    self.showModal(false);
                    notify.success('schedule removed.');
                    self.loadSchedules();
                },
                error: function () {
                    self.scheduleModalBusy(false);
                    notify.error('error to remove the schedule.');
                }
            }, id);
        };
        
        self.confirmToRemove = function () {
            notify.confirm('Do you want to delete the schedule?', function (confirmed) {
                if (confirmed) {
                    self.remove(self.scheduleModel.id());
                }
            });
        };
        
        self.update = function () {
            self.scheduleModalBusy(true);
            scheduleService.update({
                success: function (data) {
                    self.loadSchedules();
                    self.scheduleModalBusy(false);
                    self.showModal(false);
                    notify.success('schedule updated.');
                },
                error: function () {
                    self.scheduleModalBusy(false);
                    notify.error('error to update schedule.');
                }
            }, self.scheduleModel.toPostObj());
        };
        
        self.reset = function () {
            self.loadSchedule(self.scheduleModel.id());
        };
        
        self.closeModal = function () {
            self.showModal(false);
        };
        
        self.init = function() {
            $('#navMenus a[href="/Schedule/Index"]').parents('li:first').addClass('active');

            self.showSchedules();
            self.loadEvents();
            self.loadSchedules();
        };

        return self;
    });