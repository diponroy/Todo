﻿define('dataservice.schedule',
['amplify'],
function(amplify) {

    var init = function() {

        amplify.request.define('schedules.all', 'ajax', {
            url: '/schedule/all',
            dataType: 'json',
            type: 'GET'
        }),
        amplify.request.define('schedules.single', 'ajax', {
            url: '/schedule/get',
            dataType: 'json',
            type: 'GET'
        });

        amplify.request.define('schedules.create', 'ajax', {
            url: '/schedule/post',
            dataType: 'json',
            type: 'POST',
        });

        amplify.request.define('schedules.update', 'ajax', {
            url: '/schedule/put',
            dataType: 'json',
            type: 'POST',
        });

        amplify.request.define('schedules.remove', 'ajax', {
            url: '/schedule/delete',
            dataType: 'json',
            type: 'POST',
        });
    },            
        all = function(callbacks) {
            return amplify.request({
                resourceId: 'schedules.all',
                success: callbacks.success,
                error: callbacks.error
            });
        },            
        single = function(callbacks, id) {
            return amplify.request({
                resourceId: 'schedules.single',
                data: { id: id },
                success: callbacks.success,
                error: callbacks.error
            });
        },            
        create = function(callbacks, data) {
            return amplify.request({
                resourceId: 'schedules.create',
                data: data,
                success: callbacks.success,
                error: callbacks.error
            });
        },            
        update = function(callbacks, data) {
            return amplify.request({
                resourceId: 'schedules.update',
                data: data,
                success: callbacks.success,
                error: callbacks.error
            });
        },            
        remove = function(callbacks, id) {
            return amplify.request({
                resourceId: 'schedules.remove',
                data: { id: id },
                success: callbacks.success,
                error: callbacks.error
            });
        };

    init();

    return {
        all: all,
        single: single,
        create: create,
        update: update,
        remove: remove
    };
});