﻿define('dataservice.login',
['amplify'],
function(amplify) {

    var init = function() {

        amplify.request.define('login.login', 'ajax', {
            url: '/login/login',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            type: 'POST'
        });
    },
        login = function(callbacks, data) {
            return amplify.request({
                resourceId: 'login.login',
                data: JSON.stringify(data),
                success: callbacks.success,
                error: callbacks.error
            });
        };

    init();

    return {
        login: login,
    };
});