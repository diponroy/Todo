﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Todo.Utility
{
    public class PasswordUtility
    {
        public static string Encode(string originalPassword)
        {
            Byte[] encodedBytes = null;
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] originalBytes = Encoding.Default.GetBytes(originalPassword);
            encodedBytes = md5.ComputeHash(originalBytes);
            string encodedPassword = BitConverter.ToString(encodedBytes);
            return encodedPassword.Replace("-", "");
        }

        public static string GenerateRandom(int length)
        {
            char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            string password = string.Empty;
            Random random = new Random();

            for (int i = 0; i < length; i++)
            {
                int x = random.Next(1, chars.Length);
                if (!password.Contains(chars.GetValue(x).ToString()))
                    password += chars.GetValue(x);
                else
                    i--;
            }

            return password.Trim();
        }
    }
}
