﻿using System.Web.Http;
using Ninject;
using Todo.Data;

namespace Todo
{
    public class IocConfig
    {
        public static void RegisterIoc(HttpConfiguration config)
        {
            var kernel = new StandardKernel(); // Ninject IoC
            kernel.Bind<TodoUow>().To<TodoUow>();
            config.DependencyResolver = new NinjectDependencyResolver(kernel);
        }
    }
}