﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Todo
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Startup",
                url: "",
                defaults: new { controller = "Login", action = "Index" }
            );

            routes.MapRoute(
                name: "ControllerAndAction",
                url: "{controller}/{action}"
            );

            routes.MapRoute(
                name: "ControllerAndActionAndParam",
                url: "{controller}/{action}/{id}"
            );
        }
    }
}