﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Controllers.Base;
using Todo.Data;
using Todo.Model;
using Todo.Utility;

namespace Todo.Controllers
{
    public class ScheduleController : TodoController<Model.User>
    {
        TodoUow Uow { get; set; }

        public ScheduleController()
        {
            Uow = new TodoUow();
        }

        #region View
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Api
        [HttpGet]
        public ContentResult All()
        {
            var schedules = Uow.Schedules.AllActiveOrInactive().Include(x => x.Event).ToList();
            return JsonContent(schedules);
        }

        [HttpGet]
        public ActionResult Get(long id)
        {
            var entity = Uow.Schedules.AllActiveOrInactive().Where(x => x.Id == id).Include(x => x.Event).Single();
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Post(Schedule entity)
        {
            entity.AddedBy = GetLogOnSessionModel().Id;
            entity.AddedDateTime = DateTime.Now;
            Uow.Schedules.Add(entity);
            Uow.Commit();
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Put(Schedule entity)
        {
            var aEntity = Uow.Schedules.Single(entity.Id);

            aEntity.EventId = entity.EventId;
            aEntity.StartingDateTime = entity.StartingDateTime;
            aEntity.EndingDateTime = entity.EndingDateTime;
            aEntity.Remarks = entity.Remarks;
            aEntity.Status = entity.Status;
            aEntity.UpdatedBy = GetLogOnSessionModel().Id;
            aEntity.UpdatedDateTime = DateTime.Now;
            Uow.Schedules.Update(aEntity);
            Uow.Commit();
            return JsonContent(aEntity);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            var entity = Uow.Schedules.Single(id);
            entity.UpdatedBy = GetLogOnSessionModel().Id;
            entity.UpdatedDateTime = DateTime.Now;
            Uow.Schedules.Delete(entity);
            Uow.Commit();
            return JsonContent(entity);
        }
        #endregion
    }
}
