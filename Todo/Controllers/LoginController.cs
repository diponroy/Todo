﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Providers.Entities;
using Todo.Controllers.Base;
using Todo.Data;
using Todo.Utility;

namespace Todo.Controllers
{
    public class LoginController : TodoController<Model.User>
    {
        TodoUow Uow { get; set; }

        public LoginController()
        {
            Uow = new TodoUow();
        }

        #region View
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Logout()
        {
            AbandonSession();
            return View("Index");
        }
        #endregion

        #region Api
        [HttpPost]
        public ContentResult Login(Model.User entity)
        {
            bool hasLogin = false;

            entity.Password = PasswordUtility.Encode(entity.Password);
            Model.User user = Uow.Users.All().FirstOrDefault(x => x.Email == entity.Email && x.Password == entity.Password);
            if (user != null)
            {
                SetLogOnSessionModel(user);
                hasLogin = true;
            }

            return JsonContent(hasLogin);
        }
        #endregion
    }
}
