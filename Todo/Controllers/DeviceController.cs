﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Controllers.Base;
using Todo.Data;
using Todo.Model;
using Todo.Utility;

namespace Todo.Controllers
{
    public class DeviceController : TodoController<Model.User>
    {
        TodoUow Uow { get; set; }

        public DeviceController()
        {
            Uow = new TodoUow();
        }
        #region View
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Api
        [HttpGet]
        public ContentResult All()
        {
            var devices = Uow.Devices.AllActiveOrInactive().ToList();
            return JsonContent(devices);
        }

        [HttpGet]
        public ActionResult Get(long id)
        {
            var entity = Uow.Devices.Single(id);
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Post(Device entity)
        {
            entity.AddedBy = GetLogOnSessionModel().Id;
            entity.AddedDateTime = DateTime.Now;
            Uow.Devices.Add(entity);
            Uow.Commit();
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Put(Device entity)
        {
            var aEntity = Uow.Devices.Single(entity.Id);
            aEntity.Name = entity.Name;
            aEntity.Description = entity.Description;
            aEntity.Status = entity.Status;
            aEntity.UpdatedBy = GetLogOnSessionModel().Id;
            aEntity.UpdatedDateTime = DateTime.Now;
            Uow.Devices.Update(aEntity);
            Uow.Commit();
            return JsonContent(aEntity);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            var entity = Uow.Devices.Single(id);
            entity.UpdatedBy = GetLogOnSessionModel().Id;
            entity.UpdatedDateTime = DateTime.Now;
            Uow.Devices.Delete(entity);
            Uow.Commit();
            return JsonContent(entity);
        }
        #endregion
    }
}
