﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Controllers.Base;
using Todo.Data;
using Todo.Utility;

namespace Todo.Controllers
{
    public class UserController : TodoController<Model.User>
    {

        TodoUow Uow { get; set; }

        public UserController()
        {
            Uow = new TodoUow();
        }

        #region View
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Api
        [HttpGet]
        public ContentResult All()
        {
            var users = Uow.Users.AllActiveOrInactive().ToList();
            return JsonContent(users);
        }

        [HttpGet]
        public ActionResult Get(long id)
        {
            var entity = Uow.Users.Single(id);
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Post(Model.User entity)
        {
            entity.Password = PasswordUtility.Encode(entity.Password);
            entity.AddedBy = GetLogOnSessionModel().Id;
            entity.AddedDateTime = DateTime.Now;
            Uow.Users.Add(entity);
            Uow.Commit();
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Put(Model.User entity)
        {
            var aEntity = Uow.Users.Single(entity.Id);

            aEntity.Name = entity.Name;
            aEntity.ContactNo = entity.ContactNo;
            aEntity.Email = entity.Email;
            aEntity.Password = (String.IsNullOrEmpty(entity.Password))
                ? aEntity.Password
                : PasswordUtility.Encode(entity.Password);
            aEntity.Status = entity.Status;
            aEntity.UpdatedBy = GetLogOnSessionModel().Id;
            aEntity.UpdatedDateTime = DateTime.Now;
            Uow.Users.Update(aEntity);
            Uow.Commit();
            return JsonContent(aEntity);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            var entity = Uow.Users.Single(id);
            entity.UpdatedBy = GetLogOnSessionModel().Id;
            entity.UpdatedDateTime = DateTime.Now;
            Uow.Users.Delete(entity);
            Uow.Commit();
            return JsonContent(entity);
        }
        #endregion

    }
}
