﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Todo.Model.Interface;

namespace Todo.Controllers.Base
{
    public class TodoController<TSource> : Controller
    {
        private const string LogOnSession = "LogOnSession"; //session index name
        private const string ErrorController = "Error"; //session independent controller
        private const string LogOnController = "Login"; //session independent and LogOn controller    
        private const string LogOnAction = "Index"; //action to rederect

        protected TodoController()
        {
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            /*important to check both, because logOn and error Controller should be access able with out any session*/
            if (!IsNonSessionController(requestContext) && !HasSession())
            {
                //Rederect to logon action
                Rederect(requestContext, Url.Action(LogOnAction, LogOnController));
            }
        }

        private bool IsNonSessionController(RequestContext requestContext)
        {
            string currentController = requestContext.RouteData.Values["controller"].ToString().ToLower();
            var nonSessionedController = new List<string> {ErrorController.ToLower(), LogOnController.ToLower()};
            return nonSessionedController.Contains(currentController);
        }

        private void Rederect(RequestContext requestContext, string action)
        {
            requestContext.HttpContext.Response.Clear();
            requestContext.HttpContext.Response.Redirect(action);
            requestContext.HttpContext.Response.End();
        }

        protected bool HasSession()
        {
            return Session[LogOnSession] != null;
        }

        protected TSource GetLogOnSessionModel()
        {
            return (TSource) Session[LogOnSession];
        }

        protected void SetLogOnSessionModel(TSource model)
        {
            Session[LogOnSession] = model;
        }

        protected void AbandonSession()
        {
            if (HasSession())
            {
                Session.Abandon();
            }
        }

        #region Json

        public ContentResult JsonContent(object data)
        {
            string list = JsonConvert.SerializeObject(data,
                Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                });
            return Content(list, "application/json");
        }
        #endregion
    }
}
