﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Controllers.Base;
using Todo.Data;
using Todo.Model;

namespace Todo.Controllers
{
    public class EventController : TodoController<Model.User>
    {
        TodoUow Uow { get; set; }

        public EventController()
        {
            Uow = new TodoUow();
        }

        #region View
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Api
        [HttpGet]
        public ContentResult All()
        {
            var events = Uow.Events.AllActiveOrInactive().Include(x => x.Device).ToList();
            return JsonContent(events);
        }

        [HttpGet]
        public ActionResult Get(long id)
        {
            var entity = Uow.Events.Single(id);
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Post(Event entity)
        {
            entity.AddedBy = GetLogOnSessionModel().Id;
            entity.AddedDateTime = DateTime.Now;
            Uow.Events.Add(entity);
            Uow.Commit();
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Put(Event entity)
        {
            var aEntity = Uow.Events.Single(entity.Id);
            aEntity.Name = entity.Name;
            aEntity.DeviceId = entity.DeviceId;
            aEntity.Description = entity.Description;
            aEntity.Status = entity.Status;
            aEntity.UpdatedBy = GetLogOnSessionModel().Id;
            aEntity.UpdatedDateTime = DateTime.Now;
            Uow.Events.Update(aEntity);
            Uow.Commit();
            return JsonContent(aEntity);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            var entity = Uow.Events.Single(id);
            entity.UpdatedBy = GetLogOnSessionModel().Id;
            entity.UpdatedDateTime = DateTime.Now;
            Uow.Events.Delete(entity);
            Uow.Commit();
            return JsonContent(entity);
        }
        #endregion
    }
}
