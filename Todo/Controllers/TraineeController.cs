﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Controllers.Base;
using Todo.Data;
using Todo.Model;
using Todo.Utility;

namespace Todo.Controllers
{
    public class TraineeController : TodoController<Model.User>
    {
        TodoUow Uow { get; set; }

        public TraineeController()
        {
            Uow = new TodoUow();
        }

        #region Api
        [HttpGet]
        public ContentResult All(long id)
        {
            var trainees = Uow.Trainees.AllActiveOrInactive().Where(x => x.EventId == id).ToList();
            return JsonContent(trainees);
        }

        [HttpGet]
        public ActionResult Get(long id)
        {
            var entity = Uow.Trainees.Single(id);
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Post(Trainee entity)
        {
            entity.AddedBy = GetLogOnSessionModel().Id;
            entity.AddedDateTime = DateTime.Now;
            Uow.Trainees.Add(entity);
            Uow.Commit();
            return JsonContent(entity);
        }

        [HttpPost]
        public ActionResult Put(Trainee entity)
        {
            var aEntity = Uow.Trainees.Single(entity.Id);
            aEntity.Name = entity.Name;
            aEntity.ContactNo = entity.ContactNo;
            aEntity.Email = entity.Email;
            aEntity.EventId = entity.EventId;
            aEntity.Status = entity.Status;
            aEntity.UpdatedBy = GetLogOnSessionModel().Id;
            aEntity.UpdatedDateTime = DateTime.Now;
            Uow.Trainees.Update(aEntity);
            Uow.Commit();
            return JsonContent(aEntity);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            var entity = Uow.Trainees.Single(id);
            entity.UpdatedBy = GetLogOnSessionModel().Id;
            entity.UpdatedDateTime = DateTime.Now;
            Uow.Trainees.Delete(entity);
            Uow.Commit();
            return JsonContent(entity);
        }
        #endregion
    }
}
