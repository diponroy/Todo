﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Todo.Model.Enum;
using Todo.Model.Interface;

namespace Todo.Data
{
    public class EFRepository<T> where T : class, ITodoEntity
    {
        public EFRepository(DbContext dbContext)
        {
            if (dbContext == null) 
                throw new ArgumentNullException("dbContext");
            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }

        protected DbContext DbContext { get; set; }

        protected DbSet<T> DbSet { get; set; }

        public virtual IQueryable<T> All()
        {
            return DbSet;
        }

        public virtual IQueryable<T> AllActiveOrInactive()
        {
            return DbSet.Where(x => x.Status != EntityStatusEnum.Removed);
        }

        public virtual T Single(long id)
        {
            return DbSet.Single(x => x.Id == id);
        }

        public virtual void Add(T entity)
        {
            if (entity.AddedBy == null || entity.AddedDateTime == null)
            {
                throw new Exception("Credential require.");
            }
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public virtual void Update(T entity)
        {
            if (entity.UpdatedBy == null || entity.UpdatedDateTime == null)
            {
                throw new Exception("Credential require.");
            }
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }  
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            entity.Status = EntityStatusEnum.Removed;
            Update(entity);
        }
    }
}
