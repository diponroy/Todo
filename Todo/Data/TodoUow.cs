using System;
using Todo.Db.Contexts;
using Todo.Model;

namespace Todo.Data
{
    public class TodoUow : IDisposable
    {
        private TodoDbContext DbContext { get; set; }

        public EFRepository<Model.User> Users { get { return new EFRepository<User>(DbContext); } }
        public EFRepository<Device> Devices { get { return new EFRepository<Device>(DbContext); } }
        public EFRepository<Event> Events { get { return new EFRepository<Event>(DbContext); } }
        public EFRepository<Trainee> Trainees { get { return new EFRepository<Trainee>(DbContext); } }
        public EFRepository<Schedule> Schedules { get { return new EFRepository<Schedule>(DbContext); } }

        public TodoUow()
        {
            DbContext = new TodoDbContext();
            //DbContext.Configuration.ProxyCreationEnabled = false;
            DbContext.Configuration.LazyLoadingEnabled = true;
            //DbContext.Configuration.ValidateOnSaveEnabled = false;
        }
        public void Commit()
        {
            DbContext.SaveChanges();
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }
        #endregion
    }
}