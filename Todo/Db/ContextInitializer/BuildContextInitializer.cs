﻿using System;
using System.Data.Entity;
using Todo.Db.Contexts;
using Todo.Model;
using Todo.Model.Enum;
using Todo.Utility;

namespace Todo.Db.ContextInitializer
{
    internal class BuildContextInitializer : DropCreateDatabaseAlways<BuildContext>
    {
        public BuildContextInitializer()
        {
        }

        protected override void Seed(BuildContext context)
        {
            context.Users.Add(
                new Model.User()
                {
                    Name = "Admin", 
                    Email = "Admin@gmail.com",
                    Password = PasswordUtility.Encode("Admin"), 
                    Status = EntityStatusEnum.Active,
                    AddedBy = 1,
                    AddedDateTime = DateTime.Now
                });
            context.SaveChanges();
        }
    }
}