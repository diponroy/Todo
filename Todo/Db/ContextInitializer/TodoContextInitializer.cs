﻿using System.Data.Entity;
using Todo.Db.Contexts;

namespace Todo.Db.ContextInitializer
{
    internal class TodoContextInitializer : CreateDatabaseIfNotExists<Contexts.TodoDbContext>
    {
        public TodoContextInitializer()
        {
        }

        protected override void Seed(Contexts.TodoDbContext context)
        {
        }

    }
}