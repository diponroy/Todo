﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using Todo.Db.Configurations.Shared;
using Todo.Model;

namespace Todo.Db.Configurations
{
    public class TraineeConfig : TodoEntityConfig<Trainee>
    {
        public TraineeConfig()
            : base()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(150);

            Property(x => x.ContactNo)
                .IsOptional()
                .HasMaxLength(20);

            Property(x => x.Email)
                .IsOptional()
                .HasMaxLength(100);

            HasRequired(x => x.Event)
                .WithMany(l => l.Trainees)
                .HasForeignKey(x => x.EventId)
                .WillCascadeOnDelete(false);
        }
    }
}