﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using Todo.Db.Configurations.Shared;
using Todo.Model;

namespace Todo.Db.Configurations
{
    public class EventConfig : TodoEntityConfig<Event>
    {
        public EventConfig()
            : base()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(150);

            Property(x => x.Description)
                .IsOptional()
                .HasMaxLength(300);

            HasRequired(x => x.Device)
                .WithMany(l => l.Events)
                .HasForeignKey(x => x.DeviceId)
                .WillCascadeOnDelete(false);
        }
    }
}