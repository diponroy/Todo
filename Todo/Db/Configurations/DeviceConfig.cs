﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using Todo.Db.Configurations.Shared;
using Todo.Model;

namespace Todo.Db.Configurations
{
    public class DeviceConfig : TodoEntityConfig<Device>
    {
        public DeviceConfig()
            : base()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(150);

            Property(x => x.Description)
                .IsOptional()
                .HasMaxLength(300);
        }
    }
}