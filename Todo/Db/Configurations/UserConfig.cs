﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using Todo.Db.Configurations.Shared;

namespace Todo.Db.Configurations
{
    public class UserConfig : TodoEntityConfig<Model.User>
    {
        public UserConfig():base()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(150);

            Property(x => x.ContactNo)
                .IsOptional()
                .HasMaxLength(20);

            Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.Password)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}