﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using Todo.Db.Configurations.Shared;
using Todo.Model;

namespace Todo.Db.Configurations
{
    public class ScheduleConfig : TodoEntityConfig<Schedule>
    {
        public ScheduleConfig()
            : base()
        {           
        }
    }
}