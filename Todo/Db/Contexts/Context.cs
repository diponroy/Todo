﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Todo.Db.Configurations;
using Todo.Model;
using User = System.Web.Providers.Entities.User;

namespace Todo.Db.Contexts
{
    public class Context : DbContext
    {
        public DbSet<Model.User> Users { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Trainee> Trainees { get; set; }
        public DbSet<Schedule> Schedules { get; set; }

        protected Context(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new UserConfig());
            modelBuilder.Configurations.Add(new DeviceConfig());
            modelBuilder.Configurations.Add(new EventConfig());
            modelBuilder.Configurations.Add(new TraineeConfig());
            modelBuilder.Configurations.Add(new ScheduleConfig());
        }
    }
}
