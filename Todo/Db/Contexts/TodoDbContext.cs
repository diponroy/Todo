﻿using System.Data.Entity;
using Todo.Db.ContextInitializer;

namespace Todo.Db.Contexts
{
    public class TodoDbContext : Context
    {
        public TodoDbContext()
            : base(nameOrConnectionString: "DbTodo")
        {
        }

        static TodoDbContext()
        {
            //Database.SetInitializer(new TodoContextInitializer());
        }
    }
}
