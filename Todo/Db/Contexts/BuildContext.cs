﻿using System.Data.Entity;
using Todo.Db.ContextInitializer;

namespace Todo.Db.Contexts
{
    class BuildContext : Context
    {
        public BuildContext()
            : base(nameOrConnectionString: "DbTodo")
        {
        }

        static BuildContext()
        {
            Database.SetInitializer(new BuildContextInitializer());
        }
    }
}
