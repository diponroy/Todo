﻿using System.Linq;
using Todo.Db.Contexts;

namespace Todo.Db
{
    public class DbBuildInitializer
    {
        public void DropAndCreate()
        {
            using (var db = new BuildContext())
            {
                db.Users.ToList();
                db.Devices.ToList();
                db.Events.ToList();
                db.Trainees.ToList();
                db.Schedules.ToList();
            }
        }
    }
}
