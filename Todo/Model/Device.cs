﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Todo.Model.Enum;
using Todo.Model.Interface;

namespace Todo.Model
{
    public class Device : ITodoEntity
    {
        public long Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public EntityStatusEnum Status { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public long? UpdatedBy { get; set; }

        public virtual User AddedByUser { get; set; }
        public virtual User UpdatedByUser { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}