﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Todo.Model.Enum;

namespace Todo.Model.Interface
{
    public interface ITodoEntity
    {
        long Id { get; set; }
        EntityStatusEnum Status { get; set; }
        DateTime? AddedDateTime { get; set; }
        long? AddedBy { get; set; }
        DateTime? UpdatedDateTime { get; set; }
        long? UpdatedBy { get; set; }

        User AddedByUser { get; set; }
        User UpdatedByUser { get; set; }
    }
}
