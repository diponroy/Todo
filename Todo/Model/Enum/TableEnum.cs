﻿namespace Todo.Model.Enum
{
    public enum TableEnum
    {
        User,
        Device,
        Event,
        Trainee,
        Schedule,
    }
}